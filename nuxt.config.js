module.exports = {
  build: {
    // publicPath: 'assets/',
    // extractCSS: true,
    filenames: {
      vendor: 'vendor.[hash].js',
      app: 'app.[chunkhash].js',
    },
    vendor: ['axios'],
    extend (config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  head: {
    titleTemplate: 'Fight Zone',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'Meta description'}
    ]
  },
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000'
  },
  router: {
    linkActiveClass: 'active',
    middleware: []
  },
  css: [
    {
      src: '~assets/styl/main.styl', lang: 'styl'
    }
  ],
  plugins: [
    '~plugins/axios.js',
    '~plugins/element.js',
  ],
  cache: false,
  srcDir: './app/'
}
