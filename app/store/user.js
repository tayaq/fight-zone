import axios from 'axios'

export const state = () => ({
  authUser: false
})

export const mutations = {
  SET_USER: function (state, user) {
    state.authUser = user
  }
}

export const actions = {
  async auth ({commit}, {login, password}) {
    try {
      let {data} = await axios.post('api/authenticate', {
        login,
        password
      })
      commit('SET_USER', data)
    } catch (error) {
      if (error.response.status === 401) {
        throw new Error('Bad credentials')
      }
    }
  },
  async logout ({commit}) {
    await axios.post('api/logout')
    return commit('SET_USER', false)
  }
}
