import axios from 'axios'

export const state = () => ({
  viewedProducts: [2, 3]
})

export const mutations = {}

export const actions = {
  getProducts ({commit}, {type, products}) {
    return axios.get('api/getProducts', {
      params: {
        type,
        products
      }
    })
  }
}
