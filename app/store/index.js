import axios from '~plugins/axios'

export const state = () => ({
  sex: 0,
  categories: [],
  cart: []
})

export const mutations = {
  SET_SEX: function (state, sex) {
    state.sex = sex
    localStorage.setItem('sex', state.sex)
  },
  SET_CAT: function (state, categories) {
    state.categories = categories
  },
  ADD_TO_CART: function (state, cart) {
    if (typeof cart === 'object') state.cart = cart
    else {
      if (state.cart.indexOf(cart) < 0) state.cart.push(cart)
    }
    localStorage.setItem('cart', state.cart)
  }
}

export const actions = {
  async nuxtServerInit (store, {req}) {
    if (req.session && req.session.authUser) store.commit('user/SET_USER', req.session.authUser)
    await store.dispatch('categories')
  },
  async categories ({commit}) {
    let {data} = await axios.get('categories')
    commit('SET_CAT', data)
  }
}
