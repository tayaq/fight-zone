import { Router } from 'express'
import common from './common'
import product from './product'

const router = Router()

router.use(common)
router.use(product)

export default router
