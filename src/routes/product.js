import { Router } from 'express'
import query from '../db'
const router = Router()

router.get('/getProducts', (req, res, next) => {

  let queryString

  switch (req.query.type) {
    case 'new':
      queryString = `SELECT * FROM products`
      break
    case 'popular':
      queryString = `SELECT * FROM products LIMIT 1`
      break
    case 'stock':
      queryString = `SELECT * FROM products`
      break
    case 'browsed':
      queryString = `SELECT * FROM products WHERE id IN (${req.query.products})`
      break
    default:
      queryString = `SELECT * FROM products`
      break
  }

  query(queryString).then((result) => {
      res.json(result)
  })

})

export default router
