import { Router } from 'express'
import jwt from 'jsonwebtoken'
import query from '../db'
import SessionStore from 'express-mysql-session'
import { secret, dbExtend } from '../config'
const router = Router()

router.get('/categories', (req, res, next) => {

  // if(!checkPerrmission()) res.status(200).send('Access Denied')

  query(`SELECT name, link FROM category`).then((result) => {
    res.json(result)
  })

})

router.post('/authenticate', (req, res) => {

  let post = req.body
  if (post.login === 'admin' && post.password === 'admin') {
    let token = jwt.sign('', secret),
      authUser = {
          id: 1,
          login: 'tayaq',
          role: 'admin',
          token
      }
    req.session.authUser = authUser
    res.json(authUser)
  } else {
    res.status(401).json({ error: 'Bad credentials' })
  }

})

router.post('/logout', (req, res) => {

  delete req.session.authUser
  SessionStore(dbExtend).close()
  res.json({ status: 'Logout' })

})

export default router
