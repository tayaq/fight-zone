import mysql from 'mysql2'
import { dbExtend } from './config'

export const pool = mysql.createPool(dbExtend)

const query = (sql, props) => {
  return new Promise((resolve, reject) => {
    pool.getConnection((error, connection) => {
      connection.query(sql, props, (error, response) => {
          if (error) reject(error)
          else resolve(response)
        }
      )
      connection.release()
    })
  })
}

export default query