import Nuxt from 'nuxt'
import express from 'express'
import bodyParser from 'body-parser'
import session from 'express-session'
import SessionStore from 'express-mysql-session'
import helmet from 'helmet'
import { secret, dbExtend } from './config'
import api from './routes/index'

const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 3000
const app = express()

app.use(session({
  secret: secret,
  resave: true,
  saveUninitialized: false,
  store: new SessionStore(dbExtend)
}))

app.use(helmet())
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.set('port', port)
app.use('/api', api)
app.set('secret', secret)

async function start() {
  // Import and Set Nuxt.js options
  let config = require('../nuxt.config.js')
  config.dev = !(process.env.NODE_ENV === 'production')
  // Instanciate nuxt.js
  const nuxt = await new Nuxt(config)
  // Add nuxt.js middleware
  app.use(nuxt.render)
  // Build in development
  if (config.dev) {
    try {
      await nuxt.build()
    } catch (e) {
      console.error(error) // eslint-disable-line no-console
      process.exit(1)
    }
  }
  // Listen the server
  app.listen(port)
  console.log('Server listening on ' + host + ':' + port) // eslint-disable-line no-console
}

start()




